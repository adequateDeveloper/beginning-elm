{-
   The Playground module is used to experiment with various concepts in Elm programming language.
-}


module Playground exposing (..)

import Html
import Regex


escapeEarth velocity speed =
    if velocity > 11.186 then
        "Godspeed"
    else if speed == 7.67 then
        "Stay in orbit"
    else
        "Come back"


speed distance time =
    distance / time


time startTime endTime =
    endTime - startTime


revelation =
    """
    It became very clear to me sitting out there today
    that every decision I've made in my entire life has
    been wrong. My life is the complete "opposite" of
    everything I want it to be. Every instinct I have,
    in every aspect of life, be it something to wear,
    something to eat - it's all been wrong.
    """

validateEmail email =
    let
        emailPattern =
            Regex.regex "\\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}\\b"

        isValid =
            Regex.contains emailPattern email
    in
        if isValid then
            ( "Valid email", "green" )
        else
            ( "Invalid email", "red" )



{-
   main =
       Html.text <| escapeEarth 11 <| speed 7.67 <| time 2 3
-}
{-
   main =
       time 2 3
           |> speed 7.67
           |> escapeEarth 11
           |> Html.text
-}


--main =
--    Html.text revelation

main =
    validateEmail "thedude@rubix.com"
        |> toString
        |> Html.text