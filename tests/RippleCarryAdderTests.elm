module RippleCarryAdderTests exposing (allTests)

import Test exposing (describe, test)
import Expect
import RippleCarryAdder exposing (..)


allTests =
    describe "4-bit Ripple Carry Adder Components"
        [ inverterTests
        , andGateTests
        , orGateTests
        , halfAdderTests
        , fullAdderTests
        , rippleCarryAdderTests
        ]


inverterTests =
    describe "Inverter"
        [ test "output is 0 when the input is 1" <|
            \() ->
                inverter 0
                    |> Expect.equal 1
        , test "output is 1 when the input is 0" <|
            \() ->
                inverter 1
                    |> Expect.equal 0
        ]


andGateTests =
    describe "AND gate"
        [ test "output is 0 when both inputs are 0" <|
            \() ->
                andGate 0 0
                    |> Expect.equal 0
        , test "output is 0 when the first input is 0" <|
            \() ->
                andGate 0 1
                    |> Expect.equal 0
        , test "output is 0 when the second input is 0" <|
            \() ->
                andGate 1 0
                    |> Expect.equal 0
        , test "output is 1 when both inputs are 1" <|
            \() ->
                andGate 1 1
                    |> Expect.equal 1
        ]


orGateTests =
    describe "OR gate"
        [ test "output is 0 when both inputs are 0" <|
            \() ->
                orGate 0 0
                    |> Expect.equal 0
        , test "output is 1 when the second input is 1" <|
            \() ->
                orGate 0 1
                    |> Expect.equal 1
        , test "output is 1 when the first input is 1" <|
            \() ->
                orGate 1 0
                    |> Expect.equal 1
        , test "output is 1 when both inputs are 1" <|
            \() ->
                orGate 1 1
                    |> Expect.equal 1
        ]


halfAdderTests =
    describe "Half adder"
        [ describe "when both inputs are 0"
            [ test "and sum and carry-out are 0" <|
                \() ->
                    halfAdder 0 0
                        |> Expect.equal { carry = 0, sum = 0 }
            ]
        , describe "when sum is 1 and carry-out is 0"
            [ test "and the 1st input is 0 and the 2nd input is 1" <|
                \() ->
                    halfAdder 0 1
                        |> Expect.equal { carry = 0, sum = 1 }
            , test "and the 1st input is 1 and the 2nd input is 0" <|
                \() ->
                    halfAdder 1 0
                        |> Expect.equal { carry = 0, sum = 1 }
            ]
        , describe "when both inputs are 1"
            [ test "and sum is 0 and carry-out is 1 " <|
                \() ->
                    halfAdder 1 1
                        |> Expect.equal { carry = 1, sum = 0 }
            ]
        ]


fullAdderTests =
    describe "Full adder"
        [ describe "when both inputs are 0"
            [ test "and carry-in is 0 too, then both sum and carry-out are 0" <|
                \() ->
                    fullAdder 0 0 0
                        |> Expect.equal { carry = 0, sum = 0 }
            , test "but carry-in is 1, then sum is 1 and carry-out is 0" <|
                \() ->
                    fullAdder 0 0 1
                        |> Expect.equal { carry = 0, sum = 1 }
            ]
        , describe "when the 1st input is 0, and the 2nd input is 1"
            [ test "and carry-in is 0, then sum is 1 and carry-out is 0" <|
                \() ->
                    fullAdder 0 1 0
                        |> Expect.equal { carry = 0, sum = 1 }
            , test "and carry-in is 1, then sum is 0 and carry-out is 1" <|
                \() ->
                    fullAdder 0 1 1
                        |> Expect.equal { carry = 1, sum = 0 }
            ]
        , describe "when the 1st input is 1, and the 2nd input is 0"
            [ test "and carry-in is 0, then sum is 1 and carry-out is 0" <|
                \() ->
                    fullAdder 1 0 0
                        |> Expect.equal { carry = 0, sum = 1 }
            , test "and carry-in is 1, then sum is 0 and carry-out is 1" <|
                \() ->
                    fullAdder 1 0 1
                        |> Expect.equal { carry = 1, sum = 0 }
            ]
        , describe "when the 1st input is 1, and the 2nd input is 1"
            [ test "and carry-in is 0, then sum is 0 and carry-out is 1" <|
                \() ->
                    fullAdder 1 1 0
                        |> Expect.equal { carry = 1, sum = 0 }
            , test "and carry-in is 1, then sum is 1 and carry-out is 1" <|
                \() ->
                    fullAdder 1 1 1
                        |> Expect.equal { carry = 1, sum = 1 }
            ]
        ]



{-
   We picked a random and two boundary scenarios to test. Unit tests are generally useful for testing a specific scenario
   that either represents an edge case or input boundary. The first two tests verify that the rippleCarryAdder function
   generates an expected output when we add two binary numbers chosen by us at random. The last four tests verify that a
   correct output is generated when we add inputs with all 1s and 0s.

   In the last two tests, we had to use single digits to represent the binary numbers with leading zeros because
   elm-format gets rid of zeros in the front when a file is saved. The tests still work because rippleCarryAdder pads
   zeros in the front if the input number doesn’t have four digits in it.
-}


rippleCarryAdderTests =
    describe "4-bit ripple carry adder"
        [ describe "when the 1st input is 1001, and the 2nd input is 1101"
            [ test "and carry-in is 0, the output is 10110" <|
                \() ->
                    rippleCarryAdder 1001 1101 0
                        |> Expect.equal 10110
            , test "and carry-in is 1, the output is 10111" <|
                \() ->
                    rippleCarryAdder 1001 1101 1
                        |> Expect.equal 10111
            ]
        , describe "when the 1st input is 1111, and the 2nd input is 1111"
            [ test "and carry-in is 0, the output is 11110" <|
                \() ->
                    rippleCarryAdder 1111 1111 0
                        |> Expect.equal 11110
            , test "and carry-in is 1, the output is 11111" <|
                \() ->
                    rippleCarryAdder 1111 1111 1
                        |> Expect.equal 11111
            ]
        , describe "when the 1st input is 0000, and the 2nd input is 0000"
            [ test "and carry-in is 0, the output is 0000" <|
                \() ->
                    rippleCarryAdder 0 0 0
                        |> Expect.equal 0
            , test "and carry-in is 1, the output is 0001" <|
                \() ->
                    rippleCarryAdder 0 0 1
                        |> Expect.equal 1
            ]
        ]



{-
   main =
       run <|
           {-
              describe
                  "Addition"
                  [ test "1 + 1 = 2" <|
                      \() ->
                          (1 + 1) |> Expect.equal 2
                  , test "only 2 guardians have names with less than 6 characters" <|
                      \() ->
                          let
                              guardians =
                                  [ "Star-lord", "Groot", "Gamora", "Drax", "Rocket" ]
                          in
                              guardians
                                  |> List.map String.length
                                  |> List.filter (\x -> x < 6)
                                  |> List.length
                                  |> Expect.equal 2
                  ]
           -}
           {-
              describe "Less than comparison"
                  [ test "an empty list's length is less than 1" <|
                      \() ->
                          List.length []
                              |> Expect.lessThan -1
                  ]
           -}

           describe "Comparison"
               [ test "2 is not equal to 3" <|
                   \() ->
                       2 |> Expect.notEqual 3
               , test "4 is less than 5" <|
                   \() ->
                       4 |> Expect.lessThan 5
               , test "6 is less than or equal to 7" <|
                   \() ->
                       6 |> Expect.atMost 7
               , test "9 is greater than 8" <|
                   \() ->
                       9 |> Expect.greaterThan 8
               , test "11 is greater than or equal to 10" <|
                   \() ->
                       11 |> Expect.atLeast 10
               , test "a list with zero elements is empty" <|
                   \() ->
                       (List.isEmpty [])
                           |> Expect.true "expected the list to be empty"
               , test "a list with some elements is not empty" <|
                   \() ->
                       (List.isEmpty [ "Jyn", "Cassian", "K-2SO" ])
                           |> Expect.false "expected the list not to be empty"
               ]
-}
