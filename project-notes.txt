Beginning Elm
-------------
    http://elmprogramming.com/

Elm Format Editor Integration:
    https://github.com/avh4/elm-format#editor-integration

    IntelliJ auto-format:
        https://github.com/avh4/elm-format/blob/master/img/JetBrains%20setup.png

http://faq.elm-community.org/
-------------------------------------------------------------------------------

In Elm we must provide the else branch. Otherwise Elm will throw an error.

In the repl the body of the function must be indented at least one space.

Functions that take other functions as arguments or return a function are called Higher Order Functions.

Operators are Functions Too
---------------------------

    In Elm, all computations happen through the use of functions. As it so happens, all operators in Elm are functions
    too. They differ from normal functions in three ways:

        Naming

            Operators cannot have letters or numbers in their names whereas the normal functions can. +++ is an illegal
            operator in Elm, but we can legitimize it by defining it ourselves. Add the following definition right above
            main in Playground.elm

                (+++) first second =
                    first ++ second

            We have given +++ the same behavior as the ++ operator which is already defined in Elm. ++ is used to
            concatenate two strings. Notice how the operator is surrounded by parentheses in its definition.
            We have to do that when we define custom operators using the function syntax. By default, custom operators
            have the highest precedence (9) and are left-associative. Like any other built-in operator,
            we can apply +++ to its arguments.

                main =
                    Html.text ("Peanut butter " +++ "and jelly")

        Number of Arguments

            Operators accept exactly two arguments whereas there is no limit to how many arguments a normal function
            can have.

        Application Style

        Operators are applied by writing the first argument, followed by the operator, followed by the second argument.
        This style of application is called infix-style.

            > 2 + 5

        We can also apply operators in prefix-style if we so choose to.

            > (+) 2 5

Multi-line strings are delimited using """..."""

\_  - any value
\() - empty value


elm-test tests/RunTestsInTerminal.elm